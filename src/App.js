import InfoPanel from './layout/infoPanel.layout'
import './App.css';

function App() {
  return (
    <div className="App">
      <InfoPanel />
    </div>
  );
}

export default App;
